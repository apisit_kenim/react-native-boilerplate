import { connect } from 'react-redux'

import { RootScreen } from './RootScreen'

const mapStateToProps = () => ({})

const mapDispatchToProps = () => ({})

const RootContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(RootScreen)

export default RootContainer

// export * from './RootScreen'
