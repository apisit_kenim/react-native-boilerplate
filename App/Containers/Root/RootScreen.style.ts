import { StyleSheet } from 'react-native'
import { ApplicationStyles } from 'App/Theme'

export default StyleSheet.create({
  container: {
    ...ApplicationStyles.screen.container,
  },
})
