import NativeTachyons, { styles as tachyons } from 'react-native-style-tachyons'
import { StyleSheet, Dimensions } from 'react-native'

NativeTachyons.build(
  {
    /* REM parameter is optional, default is 16 */
    rem: Dimensions.get('window').width > 340 ? 18 : 16,
    /* fontRem parameter is optional to allow adjustment in font-scaling. default falls back to rem */
    fontRem: 20,
    fonts: {
      default: 'Noto Sans Thai',
    },
    colors: {
      palette: {
        white: 'white',
        black: 'black',
        // COLOR_PRIMARY: '#2ec7ab',
        // COLOR_SECONDARY: '#111',
        // COLOR_WHITE: '#FFFFFF',
        // COLOR_BLACK: '#000000',
        // COLOR_GREY: 'grey',
        // COLOR_GREEN: 'green',
        // COLOR_PLACEHOLDER: '#111111',
        // COLOR_GREY_WHITE: '#fafafa',
        // COLOR_DARK_SEPERATOR: '#d4d4d4',
        // COLOR_BLACK_TRANSP: 'rgba(0, 0, 0, 0.7)',
        // COLOR_GREY_TRANSP: 'rgba(67, 85, 85, 0.7)',
        // COLOR_BG: '#292929',
      },
    },
  },
  StyleSheet,
)

export const s = {
  ...tachyons,
}
