module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      require.resolve('babel-plugin-module-resolver'),
      {
        root: ["./App/"],
        alias: {
          "App": "./App", // optional
        }
      }
    ]
  ]
};
